UTI V3.28      �   7      /     �+  �1  �   �2     ����    $                 �          �          �          �          �                                         �          �          �          �                   �          �          �          �                   �          �          �          �       	           
                                          R+  
      �+                            �+                                                    
      �+                   �             
      �+                                                 !          "          #           $           %          &          '         (   �+     )         *   �+      +          ,          -          .   J   Tintable        Tint            1               r               a               b               g               2               3               ContainerUI     ModelPart2      Description     Cost            ItemCastsShadow LocalizedName   Comment         PropertiesList  ItemRcvShadow   TemplateResRef  UVScroll        U               V               Scroll          Stolen          Classification  Plot            Icon            Dropable        Tag             ModelPart1      ForceContainer  ModelPart3      IPActPref       DmgReduction    Cursed          GMaterial       Charges         Identified      Pickpocketable  VarTable        DescIdentified  StackSize       AppearanceSEF   ContainerPref   ModifyCost      ArmorRulesType  BaseItem        N+  �2         >+  <color=darkred><b>Climbing Manual: </b></color>
The Beginner's Guide to Scaling Sheer Surfaces
<color=blue><b>Vol: 12</b></color>
<color=blue><b>Revision: 22</b></color>
<color=blue><b>Published: 1357 DR</b></color>
<color=blue><b>By: Gaelan Bayle</b></color>

Welcome to the wold of climbing dear neophyte!  Allow me to introduce myself... I am Gaelan Bayle, intrepid master of scaling surfaces both sheer and craggy. I am uniquely qualified to be your instructor, having spent decades perfecting my... "craft" along the length of the Sword Coast.  From conquering peaks to burgling mansions - and even extricating myself from the occasional pit-trap - few who live can match my expertise in this area!


<color=darkred><b>Tools:</b></color>
_________________

First off, if you want to climb, you'll need the proper tools.  While climbing a wall free-form is a great way to impress the local strumpets, it's also a fine way to snap one's cocky neck.  Fair warning friend... unless you have climbing tools, you won't likely be scaling much of anything.  Therefore, I'd advise you leave daredevil stunts and other related tomfoolery to the soon-to-be-crippled, the clinically insane or the truly desperate.

There are four varieties of climbing gear available, which we'll cover below:

<color=blue><b>Nothing:</b></color>
- Skill Check Bonus: -30
- Cost: Nothing
I did warn you about attempting climbs without a rope - did I not?  So you're intent on trying anyways?  Ah well... you were warned.  Anyone attempting to scale a surface absent climbing aids will suffer a huge penalty to their attempt!  If one were keen on making unaided climbs habitual, they'd be wise to get their affairs in proper order.
- Pro: No rope required.
- Pro: Anyone can ATTEMPT it.
- Con: Have a Healer on standby.

<color=blue><b>Rope:</b></color>
- Skill Check Bonus: +5
- Cost: 15 Gold Pieces
This is a length of stout hemp rope. With it, one can attempt to climb free of pit traps or up sheer surfaces.  After ten uses, the rope will wear out and become unsafe to use.

Though not the equal of proper climbing tools, (which only rogues, mountaineers and professional burglars are versed in), this rope still makes it possible to climb otherwise inescapably deep pits or reach high locations.  A rope gives its user a +5 bonus to climbing checks.
- Pro: Anyone can make use of rope!
- Pro: Dirt cheap!
- Con: A greater chance of falling!

<color=blue><b>Silk Rope, Pitons & Climbing Harness:</b></color>
- Skill Check Bonus: +10
- Cost: 30 Gold Pieces
This is a length of supple silk rope, mithral pitons and a climbing harness.  A character may use them it to scale sheer surfaces - or the walls of a pit trap.  After ten uses, the tools will wear out, becoming too dangerous to deploy.

Due to the gear's light weight and sturdy construction, the equipment is perfect for burgling or scaling walls - granting a +10 bonus to the climbing attempt.  However, only rogues, mountain climbers and cat-buglers are properly versed in its usage.  Other untrained classes merely gain the +5 benefit of a standard rope.
- Pro: Greatly reduced chances of falling.
- Con: More expensive than rope.
- Con: Requires specialized training (Rogue or Bard Levels).

<color=blue><b>Rope of Climbing:</b></color>
- Skill Check Bonus: +15
- Cost: 60,000 Gold Pieces
This is a length of magical spider-silk rope.  Its owner may use it to climb free of pit traps or up sheer surfaces.  Unlike normal ropes, this mystical cord  actively helps a trained user climb, removing the need for pitons and a harness.  Additionally, it never seems to show signs of wear - even with extended use.

This enchanted rope is perfect for burgling or scaling walls, granting its user a +15 bonus to all climbing attempts.  However, the cordage's power only works for rogues - its greater magical properties refuse to operate for other classes, acting as regular rope. (+5 to climb checks).
- Pro: Almost impossible to fall.
- Con: Exceedingly rare.
- Con: Astronomically expensive.
- Con: Requires specialized training (Rogue or Bard Levels).


<color=darkred><b>How to Climb:</b></color>
_________________

Now that we've covered the essential tools, let's discuss mechanics my dear readers!  Pay close attention and we'll have you scaling surfaces like a billy-goat in no time.  Or at very least, reduce your chances of an express ride to Kelemvore's court!

<color=blue><b>What Makes Climbing Impossible:</b></color>
- Over-Encumbrance: If you're carrying too much junk to move, you can't climb up a sheer surface!

- Offhand Weapon, Item, of Shield: You MUST have a free hand to use your climbing tools!

<color=blue><b>What Makes Climbing Difficult:</b></color>
- Encumbered: If you're role-playing a pack-mule and cart, scaling walls is difficult, incurring a -3 penalty.

- Wearing Armor: Wearing armor makes climbing difficult!  Light/Medium/Heavy Armors incurs a penalty of -2/-4/-7 to all climbing checks.

- Climbing Without a Proper Tools: Not using a climbing aid will give you a whopping -30 on your climbing check!

<color=blue><b>What Makes Climbing Easier:</b></color>
- Class: Some classes are trained in the art of climbing.  Rogues and Bards are quite adept at tackling vertical inclines, gaining a +5 bonus.

- Race: A few races are naturally skilled at scaling sheer surfaces.  For example, Halflings are very capable climbers, gaining a +2 bonus.

- Climbing Gloves: These gloves are a combination of supple rayskin and gritty sharkskin on the palms and fingertips.  When worn, they grant a +2 bonus on attempts to scale sheer surfaces or use climbing gear. Only rogues, mountain climbers and burglars understand how to properly leverage these gloves.  Other classes will not gain any benefit from them.

- Cat-burglar's Attire: These dusky silk outfits are designed with thieves in mind.  Their deep colors blend well into the night's shadows, and they are covered with loops, hooks and slings to snugly holding ropes, pitons and lock-picking tools.  It takes specific training use this equipment however, and thus, only rogues will benefit from the +5 climbing bonus while wearing them.

- Ring of Feather Falling: This ring is crafted with a feather pattern all around its edge. It acts exactly like a feather fall spell, activated immediately if the wearer falls more than five feet. 

<color=darkred><b>Chances to Climb a Surface:</b></color>
_________________

So what are the odds of scaling a rock wall or pulling yourself free of a pit?  A wise "professional" should always calculate their chance of failure when attempting to climb a surface - one must always properly gauge risk v.s. reward.  (Unless they plan on opening an expense account with Ilmater's clergy).

<color=blue><b>No Pressure - Take Ten Rule:</b></color>  
Take Ten applies to any ability or skill check that involves a catastrophic result of failure.  A character adds their Strength Modifier to ten and this is considered "the roll."  This assumes the person is taking their time, being cautious and acting methodically.  If the "Take Ten" is higher than the Climbing DC, the character automatically climbs up or down a surface.

<color=blue><b>Under Pressure - Climbing Check:</b></color>  
If a Take Ten  cannot succeed OR if a character is engaged in combat, they will have to roll a Climb Check.  They must roll d20 and add their Strength Modifier to the attempt.  If they succeed, they may climb up or down.  Should they fail...


<color=darkred><b>Consequences - What happens when you Screw Up and Fall:</b></color>
_________________

Ahh, so you've bought the best gear, spent time honing your skills, and decided to shinny up a granite wall.  One frown from Waukeen later, (coupled with a slick patch of moss), and the cliff-face is sailing past you at an alarming pace.  Upside? Congratulations on your discovery of unaided manned flight!  Downside? Your speed is terminal and your maneuverability is restricted to a single direction - down.  

What now?  Well... to be blunt - pun intended - you're about to get hurt.  Probably significantly.  I sincerely hope you're on speaking terms with some manner of priest.  Fortunately for the beginning climber, not all screw-ups are crated equal - the height at which the unplanned decent begins may be  the difference between "a few busied ribs" and "role-playing an ocher jelly."

<color=blue><b>Damage:</b></color>
Falling damage is measured in increments of 1d6 per ten feet fallen - with a ceiling of 20d6, (for a two-hundred foot plunge).  Anything beyond two-hundred feet will kill anyone not packing some serious magical firepower.

<color=blue><b>Minor Failure:</b></color>
If a player misses a check by five or less, they make it halfway up, and fall HALF the height.  For example: If the height is sixty feet, then the unfortunate sap will tumble head over-heels for thirty feet, sustaining 3d6 damage.

<color=blue><b>Critical Failure:</b></color>
If any character misses their check by 6 or more, they have fallen the whole distance.  Example:  If the height is one-hundred-and-twenty feet, our sentient meteorite will suffer 12d6 damage on impact.

<color=blue><b>Falling While Climbing Upwards:</b></color>
If a character fails their Climbing Check while scaling up a surface, they end up back where they started.  They must then decide of they wish to continue attempting ascents, or exercise some good judgement and give up.

<color=blue><b>Falling While Climbing Downwards:</b></color>
The positive side of climbing downward is that arrival at the target destination is one-hundred-percent assured!  On the negative end of the spectrum, no guarantees are made on the condition of the climber once they reach the landing zone.  Any attempt to climb down will always result in successfully reaching the ground.  (One way or another).


<color=darkred><b>Conclusion:</b></color>
_________________

Well that about wraps it up!  I hope you found this guide helpful and enlightening!  Go grab yourself some tools and break a leg!  Or preferably... don't!
<color=blue><b>- Gaelan Bayle</b></color>


<color=green>DISCLAIMER: Please note that nothing provided in this manual is considered actual climbing training, and Gaelan Bayle, his publisher and the Shadow Thieves of Amn are in no way liable for any fractures, contusions, concussions, compound breakage, decapitation, liquefaction, pulping, defenestration, dismemberment, disembowelment or any other variety of injury sustained while following the non-advice provided here-in.   This tome is intended for entertainment and educational purposes only - and anyone scaling sheer surfaces is out of their ever-loving skull. Read: It's not our fault when they die.  Horribly.  Seriously though - don't attempt to sue us.  We will kill you.  No... we don't mean metaphorically.  We will send Arkanis out with a portrait of you, and he will slip nightshade into your haggis.</color>+   �2            Illustrated Climbing Manual    book97   Climbing Gear   book97   ����                                                 !   "   #   $   %   &   '   (   )   *   +   ,   -   .   /   0   1   2   3   4   5   6                           	   
                                       