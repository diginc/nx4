// i_mca_magic_quiver_13_ac
// by Sabranic
// 2/12/16

// Distributed with MotB Makeover SoZ Edition by Brendan Bellina

// Creates a stack of 50 Vampiric arrows 1 time per day
// This script is executed automatically when the item is activated
// To trigger properly the name of the script MUST be "i_" + itemtag + "_ac"


void main()
{
	object oPC = GetItemActivator();
	object oITEM = CreateItemOnObject("mca_create_arrow_13", oPC, 50, "mca_create_arrow_13", 0);
	SetFirstName(oITEM, "Conjured Vampiric Arrow");
	SetTag(oITEM, "mca_create_arrow_13");
	SetDescription(oITEM, "These arrows have been magically summoned from an enchanted quiver.");
}

CreateItemOnObject