// i_mca_magic_bullets_02_ac
// by Sabranic
// 2/15/16

// Distributed with MotB Makeover SoZ Edition by Brendan Bellina

// Creates a stack of 50 +1 bullets 1 time per day
// This script is executed automatically when the item is activated
// To trigger properly the name of the script MUST be "i_" + itemtag + "_ac"


void main()
{
	object oPC = GetItemActivator();
	object oITEM = CreateItemOnObject("mca_create_bullet_2", oPC, 50, "mca_create_bullet_2", 0);
	SetFirstName(oITEM, "Conjured Bullet +1");
	SetTag(oITEM, "mca_create_bullet_2");
	SetDescription(oITEM, "These bullets have been magically summoned from an enchanted bullet bag.");
}

CreateItemOnObject