// i_mca_magic_boltcase_01_ac
// by Sabranic
// 2/15/16

// Distributed with MotB Makeover SoZ Edition by Brendan Bellina

// Creates a stack of 50 bolts 1 time per day
// This script is executed automatically when the item is activated
// To trigger properly the name of the script MUST be "i_" + itemtag + "_ac"


void main()
{
	object oPC = GetItemActivator();
	object oITEM = CreateItemOnObject("mca_create_bolt_1", oPC, 50, "mca_create_bolt_1", 0);
	SetFirstName(oITEM, "Conjured Bolt");
	SetTag(oITEM, "mca_create_bolt_1");
	SetDescription(oITEM, "These bolts have been magically summoned from an enchanted quiver.");
}

CreateItemOnObject