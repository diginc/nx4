//	m_c_tw_camp
/*
	Checks to see if the party qualifies for the Camp Routine teamwork benefit.
	
	100 - Check to see the PC speaker Survival 8+.
	200 - Check to see if all other party members have Survival 1+.
*/
//	JSH-OEI 7/28/08

int StartingConditional(int nCase)
{
	object oPC				= GetPCSpeaker();
	object oPartyMember		= GetFirstFactionMember(oPC, FALSE);
	
	
	switch (nCase)
	{
		case 100:	// Camp Routine - Leader Prerequisite
			if (GetSkillRank(SKILL_SURVIVAL, oPC, TRUE)<8)
				return TRUE;
			else	
				return FALSE;
					
		case 200:	// Camp Routine - Team Member Prerequisite
			while (GetIsObjectValid(oPartyMember))
			{
				/*	All other party members must meet this requirement.	*/
				if ((oPartyMember != oPC) && (GetIsRosterMember(oPartyMember) || GetIsOwnedByPlayer(oPartyMember)))
				{
					if (GetSkillRank(SKILL_SURVIVAL, oPartyMember, TRUE)<1)
						return TRUE;
				}
				oPartyMember	= GetNextFactionMember(oPC, FALSE);
			}
			return FALSE;
					
		break;
	}
	
	return FALSE;
}