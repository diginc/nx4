void main(object oSTONE)
{
    object oPC = OBJECT_SELF;
    if (GetIsInCombat(oPC)==TRUE)
    {
        FloatingTextStringOnCreature("You cannot use the Stone of Recall while you are in combat.", oPC, FALSE);
        int nCHARGE = GetItemCharges(oSTONE);
        SetItemCharges(oSTONE, nCHARGE + 1);
        return;
    }
    object oWAY = GetObjectByTag("teleport_waypoint");
    location lWAY = GetLocation(oWAY);
    object oTARGET = GetFirstFactionMember(oPC);
    while (oTARGET != OBJECT_INVALID)
    {
        AssignCommand(oTARGET, JumpToLocation(lWAY));
        oTARGET = GetNextFactionMember(oPC);
    }
}