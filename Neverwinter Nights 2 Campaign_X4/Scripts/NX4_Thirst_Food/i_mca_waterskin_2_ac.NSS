// 'i_mca_waterskin_2_ac'
//
// Tag-based script for Activation of the enchanted waterskin.

#include "thirst_inc"


void main()
{
//	Tell("run ( i_mca_waterskin_2_ac )");
	object oWaterskin = GetItemActivated();
	object oPC = GetItemActivator();
	UpdateThirstPoints(oPC, 15.f);
}