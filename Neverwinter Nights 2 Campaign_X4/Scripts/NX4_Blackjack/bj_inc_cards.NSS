/**
 * The basic deck are CARD_0, CARD_1, ... CARD_52 with
 * signify Ace, 2-10, Jack, Queen, King of
 * Clubs/Hearts/Spades/Diamonds in that order
 *
 * Copyright (C) 2002-2003 Jim Woodgate - woody@realtime.net
 */

//modified 8/2007 by Pat Ransom for NWN2 
 
int    NUM_DECKS = 1;

string SUIT0 = "C";
string LSUIT0 = "Clubs";
string SUIT1 = "H";
string LSUIT1 = "Hearts";
string SUIT2 = "S";
string LSUIT2 = "Spades";
string SUIT3 = "D";
string LSUIT3 = "Diamonds";

string CARD0 = "A";
string LCARD0 = "Ace";
string CARD1 = "2";
string LCARD1 = "Deuce";
string CARD2 = "3";
string LCARD2 = "Three";
string CARD3 = "4";
string LCARD3 = "Four";
string CARD4 = "5";
string LCARD4 = "Five";
string CARD5 = "6";
string LCARD5 = "Six";
string CARD6 = "7";
string LCARD6 = "Seven";
string CARD7 = "8";
string LCARD7 = "Eight";
string CARD8 = "9";
string LCARD8 = "Nine";
string CARD9 = "T";
string LCARD9 = "Ten";
string CARD10 = "J";
string LCARD10 = "Jack";
string CARD11 = "Q";
string LCARD11 = "Queen";
string CARD12 = "K";
string LCARD12 = "King";

void shuffle(object oStore) {
  int i;
  for (i=0; i<52; i++) {
    SetLocalInt(oStore, "CARD_"+IntToString(i), 0);
  }
}

int cardsLeft(object oStore) {
  int i;
  int left=0;

  for (i=0; i<52; i++) {
    left += (NUM_DECKS - GetLocalInt(oStore, "CARD_"+IntToString(i)));
  }

  return left;
}

int quickCheck(object oStore) {
  int i;
  for (i=0; i<52; i++) {
    if (GetLocalInt(oStore, "CARD_"+IntToString(i)) < NUM_DECKS)
      return TRUE;
  }

  return FALSE;
}

int pickCard(object oStore) {
  int left = quickCheck(oStore);
  if (left == FALSE) {
    PrintString("*ERROR* Ran out of cards!");
    return -1;
  }

  int pick = Random(52);
  while (GetLocalInt(oStore, "CARD_"+IntToString(pick)) == NUM_DECKS) {
    pick = Random(52);
  }

  SetLocalInt(oStore, "CARD_"+IntToString(pick),
              GetLocalInt(oStore, "CARD_"+IntToString(pick))+1);
  return pick;
}

string getValue(int card) {
  switch(card%13) {
    case 0: return CARD0;
    case 1: return CARD1;
    case 2: return CARD2;
    case 3: return CARD3;
    case 4: return CARD4;
    case 5: return CARD5;
    case 6: return CARD6;
    case 7: return CARD7;
    case 8: return CARD8;
    case 9: return CARD9;
    case 10: return CARD10;
    case 11: return CARD11;
    case 12: return CARD12;
    default: return "Invalid Card: "+IntToString(card);
  }
  return "Invalid Card: "+IntToString(card);
}

string getLongValue(int card) {
  switch(card%13) {
    case 0: return LCARD0;
    case 1: return LCARD1;
    case 2: return LCARD2;
    case 3: return LCARD3;
    case 4: return LCARD4;
    case 5: return LCARD5;
    case 6: return LCARD6;
    case 7: return LCARD7;
    case 8: return LCARD8;
    case 9: return LCARD9;
    case 10: return LCARD10;
    case 11: return LCARD11;
    case 12: return LCARD12;
    default: return "Invalid Card: "+IntToString(card);
  }
  return "Invalid Card: "+IntToString(card);
}

string getSuit(int card) {
  switch(card/13) {
    case 0: return SUIT0;
    case 1: return SUIT1;
    case 2: return SUIT2;
    case 3: return SUIT3;
    default: return "Invalid Card: "+IntToString(card);
  }
  return "Invalid Card: "+IntToString(card);
}

string getLongSuit(int card) {
  switch(card/13) {
    case 0: return LSUIT0;
    case 1: return LSUIT1;
    case 2: return LSUIT2;
    case 3: return LSUIT3;
    default: return "Invalid Card: "+IntToString(card);
  }
  return "Invalid Card: "+IntToString(card);
}

// Since we don't have an easy way to prepend 0's
// on an in, simple card serializer
string cardSer(int card) {
  if (card < 10)
    return "0"+IntToString(card);
  return IntToString(card);
}

int calculateTotal(string hand) {
  int total = 0;
  int aces = 0;
  int i;
  for (i=0; i<GetStringLength(hand); i+=2) {
    int value = StringToInt(GetSubString(hand, i, 2))%13;

    if (value == 0) {
      aces++;
      total+=11;
    }
    else if (value < 10) {
      total += value+1;
    }
    else {
      total += 10;
    }
  }

  while (total > 21 && aces > 0) {
    aces--;
    total -= 10;
  }
  return total;
}

// Given a hand like 012513, turn it into text
// like 2C KH AS
string getFullHand(string hand) {
  string fullName;
  int i;
  for (i=0; i<GetStringLength(hand); i+=2) {
    int card = StringToInt(GetSubString(hand, i, 2));
    fullName += getLongValue(card)+" of "+getLongSuit(card)+", ";
  }
  return fullName;
}