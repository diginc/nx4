#include "bj_inc_cards"

int StartingConditional(int nShowDealerHole)
{
	
	string sPlayerHand = GetLocalString(OBJECT_SELF, "BJPLYRHAND");
		
	if (nShowDealerHole)
	{
		string dealerHand = GetLocalString(OBJECT_SELF, "BJDLRHAND");
		int total = calculateTotal(dealerHand);
		SetCustomToken(540, getFullHand(dealerHand) + "Total: " + IntToString(total));
	}
	else 
	{
		int nDealerFace = GetLocalInt(OBJECT_SELF, "BJDLRFACE");	
		SetCustomToken(540, getLongValue(nDealerFace)+" of "+getLongSuit(nDealerFace));
	}
	SetCustomToken(520, getFullHand(sPlayerHand) + "Total: " + IntToString(calculateTotal(sPlayerHand)));
	return TRUE;
}