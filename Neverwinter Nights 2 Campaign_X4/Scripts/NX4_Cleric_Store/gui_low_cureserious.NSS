const int PRICE = 120;
const int PARTY = FALSE;
const string MESSAGE = "Not enough gold";

void main(int iTarget)
{
	object oTarget = IntToObject(iTarget);
	effect eLink = EffectLinkEffects(EffectVisualEffect(VFX_IMP_HEALING_S), EffectHeal(d8(3)+7));

	if (GetGold(oTarget) >= PRICE)
	{
		if(PARTY)
		{
			object oCurrent = GetFirstFactionMember(oTarget, FALSE);
			while(GetIsObjectValid(oCurrent))
			{
				ApplyEffectToObject(DURATION_TYPE_INSTANT, eLink, oCurrent);
				oCurrent = GetNextFactionMember(oTarget, FALSE);
			}
		}
		else
			ApplyEffectToObject(DURATION_TYPE_INSTANT, eLink, oTarget);	
		
		TakeGoldFromCreature(PRICE, oTarget, TRUE);
	}
	else
		SendMessageToPC(oTarget, MESSAGE);
}