//standing dance male unbumpable
#include "ginc_misc"
#include "ginc_math"
#include "ginc_wp"

void PlayCustomLoopingAnimation(object oObject, string sAnimationName)
{
	PlayCustomAnimation(oObject, sAnimationName, 1);
}

void PlayCustomOneShotAnimation(object oObject, string sAnimationName)
{
	PlayCustomAnimation(oObject, sAnimationName, 0);
}

void main()
{
	
	
	if (GetAILevel(OBJECT_SELF) == AI_LEVEL_VERY_LOW)
	{
		return;
	}

	object oActor = OBJECT_SELF;
	
	if (GetLocalInt(oActor, "SetDefaultAnimation") == 0)
	{
		AssignCommand(oActor, PlayCustomLoopingAnimation(oActor, "sitdrinkidle"));
		SetLocalInt(oActor, "SetDefaultAnimation", 1);
	}
	
	int nRandom = RandomIntBetween(1, 9);
	
	switch(nRandom)
	{
		case 1:
			PlayCustomLoopingAnimation(oActor, "dance02");
			break;
		case 3:
			PlayCustomLoopingAnimation(oActor, "dance02");
			break;
		case 4:
			PlayCustomLoopingAnimation(oActor, "clapping");
			break;
		case 5:
			PlayCustomLoopingAnimation(oActor, "curtsey");
			break;
		case 6:
			PlayCustomLoopingAnimation(oActor, "flirt");
			break;
		case 7:
			PlayCustomLoopingAnimation(oActor, "dance02");
			break;
	}
	
	if (!GetLocalInt(OBJECT_SELF,"do_once")){
	
		SetLocalInt(OBJECT_SELF,"do_once",TRUE);
		SetLocalFloat(OBJECT_SELF,"heading",GetFacing(OBJECT_SELF));
		}
	if(GetFacing(OBJECT_SELF)!=GetLocalFloat(OBJECT_SELF,"heading")){
	
		SetFacing(GetLocalFloat(OBJECT_SELF,"heading"),TRUE);
	}
	
}