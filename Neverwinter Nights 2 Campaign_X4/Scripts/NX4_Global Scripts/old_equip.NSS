#include "ginc_debug"
#include "ginc_item"
const int STR_REF_NOT_ALLOWED	= 210757;

void main()
{
	object oPC = GetPCItemLastEquippedBy();
	ExecuteScript("eq_" + GetTag(GetPCItemLastEquipped()), oPC);
	// TOH CODE ENDS HERE
	
   	object oItem 	= GetPCItemLastEquipped();
		
	int bEquipForbidden = FALSE;
	// construct can't equip items (except hidden creature items)
	string sTag = GetTag(oPC);
	if ((sTag == "construct")
		||(sTag == "okku")
		||(sTag == "a04_golem_ally")
		||(sTag == "oneofmany")
		)
	{
		int nSlot = GetSlotOfEquippedItem(oItem, oPC);
		// all non-creature items forbidden by default
		if (!GetIsCreatureSlot(nSlot))
		{
			bEquipForbidden = TRUE;
		}		
		
		// okku allowed to wear rings and amulets
		if (sTag == "okku")
		{
			if ((nSlot == INVENTORY_SLOT_NECK) 
				|| (nSlot == INVENTORY_SLOT_LEFTRING)
				|| (nSlot == INVENTORY_SLOT_RIGHTRING))
			{				
				bEquipForbidden = FALSE;
			}
		}	
	}
	
	if (bEquipForbidden == TRUE)
	{
		AssignCommand(oPC, ActionUnequipItem(oItem));
		SendMessageToPCByStrRef(oPC, STR_REF_NOT_ALLOWED);
		return;
	}
	
	ExecuteScript("x2_mod_def_equ", OBJECT_SELF);
}

