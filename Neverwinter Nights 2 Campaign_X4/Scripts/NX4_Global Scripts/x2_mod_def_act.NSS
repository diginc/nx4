//::///////////////////////////////////////////////
//:: x2_mod_def_act
//:://////////////////////////////////////////////
/*
	OnItemActivate Event
*/

#include "x2_inc_switches"


void main()
{
//	SendMessageToPC(GetFirstPC(FALSE), "run ( x2_mod_def_act )");

	object oPC = GetItemActivator();
	object oItem = GetItemActivated();

	string sTag = GetTag(oItem);
	if (sTag == "recall_stone")
	{
		AddScriptParameterObject(oItem);
		ExecuteScriptEnhanced("script_teleport_stone", oPC);
		return;
	}

	ExecuteScript("ac_" + sTag, oPC);


	// * Generic Item Script Execution Code
	// * If MODULE_SWITCH_EXECUTE_TAGBASED_SCRIPTS is set to TRUE on the module,
	// * it will execute a script that has the same name as the item's tag
	// * inside this script you can manage scripts for all events by checking against
	// * GetUserDefinedItemEventNumber(). See x2_it_example.nss
	if (GetModuleSwitchValue(MODULE_SWITCH_ENABLE_TAGBASED_SCRIPTS))
	{
		SetUserDefinedItemEventNumber(X2_ITEM_EVENT_ACTIVATE);

		int iRet = ExecuteScriptAndReturnInt(GetUserDefinedItemEventScriptName(oItem), OBJECT_SELF);
		if (iRet == X2_EXECUTE_SCRIPT_END)
		{
			return;
		}
	}
}
