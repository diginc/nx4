// b05_a_activate_disposal_portal
/*
	Description: When disposal portal is activated, this script will check to see if the gargoyles
	are in the arcane circle by checking bGargoylesInDisposal and if they are, destroy the gargoyles 
	while  adding 20d6 fire damage to any other creature. If not it will add 20d6 fire damage to any 
	creature in the arcane circle. If the object is an item it will check the plot flag. If that is
	true it will simply play fire on the item, otherwise the item will be destroyed. Trigger is drawn
	around arcane circle so that there is an accurate representation of it when we grab objects from 
	in circle.
	
*/
// MDiekmann - 5/16/07
// JSH-OEI 7/2/07: Portal effect now uses new fiery song portal effect.

void main()
{
	object oCircleTrigger 		= 	GetObjectByTag("CircleTrigger"); 
	object oArcaneCircle		=	GetObjectByTag("sp_fire_elemental");
	object oTarget;
	location lArcaneCircle 		= 	GetLocation(oArcaneCircle);
	effect	eArcaneCircle		=	EffectVisualEffect(VFX_HIT_SPELL_FLAMESTRIKE);
	effect  eFire				= 	EffectVisualEffect(VFX_IMP_FLAME_M);
	effect eDam;
	effect eLink;
	int nDam;
	int bGargoylesInDisposal 	= 	GetLocalInt(GetModule(), "bGargoylesInDisposal");
	float fDelay 				= 	0.2;
	float fDelayDestroy			=	1.0;
	
	// Create portal and then destroy it a few seconds later	
	location lDoor = GetLocation(GetObjectByTag("b05_sp_door3"));
	object oPortalFX = CreateObject(OBJECT_TYPE_PLACED_EFFECT, "b05_fx_portal_disposal", lDoor);
	DestroyObject(oPortalFX, 6.0);
	
	//Regardless of what happens play fire effect on portal and circle.
	ApplyEffectAtLocation(DURATION_TYPE_INSTANT, eArcaneCircle, lArcaneCircle); 
	
	//Get first object in circle of type creature or item
	oTarget = GetFirstInPersistentObject(oCircleTrigger, OBJECT_TYPE_CREATURE | OBJECT_TYPE_ITEM );
	
	while(GetIsObjectValid(oTarget))
	{   
		//if a creature...    
		if(GetObjectType(oTarget) == OBJECT_TYPE_CREATURE)
		{	
			//check to see if gargoyles are there...
			if(bGargoylesInDisposal)
			{
				//if yes then check to see if object is a gargoyle
				//if(GetLocalInt(oTarget, "bIsGargoyle") == TRUE)
				if(GetTag(oTarget) == "b05_gargoyle_1" || GetTag(oTarget) == "b05_gargoyle_2" || GetTag(oTarget) == "b05_gargoyle_3")
				{
					//if it is then play fire effect, death conversation and then destroy
					BeginConversation("b05_gargoyle_death", oTarget);
					DelayCommand(fDelay, ApplyEffectToObject(DURATION_TYPE_INSTANT, eFire, oTarget));
					DelayCommand(fDelayDestroy, DestroyObject(oTarget));
					SetLocalInt(GetModule(), "bGargoylesInDisposal", 0);
				}
				//otherwise deal 20d6 fire damage and play fire effect
				else
				{
					nDam = d20(6);
					eDam = EffectDamage(nDam, DAMAGE_TYPE_FIRE);
					eLink = EffectLinkEffects(eFire, eDam);
					DelayCommand(fDelay, ApplyEffectToObject(DURATION_TYPE_INSTANT, eLink, oTarget));	
				}
			}
			//if gargoyle flag not set deal 20d6 fire damage and play fire effect on any creature
			else
			{
				nDam = d20(6);
				eDam = EffectDamage(nDam, DAMAGE_TYPE_FIRE);
				eLink = EffectLinkEffects(eFire, eDam);
				DelayCommand(fDelay, ApplyEffectToObject(DURATION_TYPE_INSTANT, eLink, oTarget));
			}
		}
		//if an item
		else if(GetObjectType(oTarget) == OBJECT_TYPE_ITEM)
		{
			//check to see if a plot item
			if(GetPlotFlag(oTarget))
			{
				//if it is play fire effect and don't hurt it
				DelayCommand(fDelay, ApplyEffectToObject(DURATION_TYPE_INSTANT, eFire, oTarget));
			}
			//otherwise play fire effect and kill it.
			else
			{
				DelayCommand(fDelay, ApplyEffectToObject(DURATION_TYPE_INSTANT, eFire, oTarget));
				DelayCommand(fDelayDestroy, DestroyObject(oTarget));
			}
		}
		//get the next object
		oTarget = GetNextInPersistentObject(oCircleTrigger, OBJECT_TYPE_CREATURE | OBJECT_TYPE_ITEM );
	}
}