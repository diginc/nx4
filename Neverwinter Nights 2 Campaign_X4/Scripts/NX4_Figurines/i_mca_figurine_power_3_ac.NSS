// 'i_mca_figurine_power_3_ac'
//
// Onyx Panther
// Script to summon and buff a creature.
// Tag-based (module-level) OnItemActivated script.
// - designed by Sabranic
// - scripted by kevL
// ed.2016 dec 16

#include "i_mca_figurine_inc"

// ________________
// ** CONSTANTS ***
// ----------------
// resref of creature
const string RESREF = "special_summon_4";
// 1 minute per pip
const int DURATION = 45;


// ___________
// ** MAIN ***
// -----------
// - OBJECT_SELF is Module.
void main()
{
    AssignCommand(GetItemActivator(), summon(RESREF, DURATION));
}