// 'i_mca_figurine_power_10_ac'
// Jade Hound
// Tag-based (module-level) OnItemActivated script.

// ________________
// ** CONSTANTS ***
// ----------------
// resref of creature
const string RESREF = "special_summon_5";
// 1 minute per pip
const int DURATION = 45;


// ___________________
// ** DECLARATIONS ***
// -------------------

// Summons a creature.
void summon();
// Buffs the summoned creature.
void buff();


// ___________
// ** MAIN ***
// -----------
// OBJECT_SELF is Module.
void main()
{
    AssignCommand(GetItemActivator(), summon());
}


// __________________
// ** DEFINITIONS ***
// ------------------

// Summons a creature.
// OBJECT_SELF is ItemActivator: the effect must be created/applied by a member
// of the party that the summoned-associate will join.
void summon()
{
    string sResref = RESREF;
    effect eSummon = EffectSummonCreature(sResref, VFX_FNF_SUMMON_MONSTER_3, 0.3f, FALSE);

    location lTarget = GetItemActivatedTargetLocation();
    ApplyEffectAtLocation(DURATION_TYPE_TEMPORARY, eSummon, lTarget, TurnsToSeconds(DURATION));

    DelayCommand(2.f, buff());
}

// Buffs the summoned creature.
// OBJECT_SELF is ItemActivator.
void buff()
{
    object oPet = GetAssociate(ASSOCIATE_TYPE_SUMMONED);

    int hpCur = GetCurrentHitPoints(oPet);
    int hpMax = GetMaxHitPoints(oPet);

    int hpHeal = hpMax - hpCur;
    if (hpHeal > 0)
    {
        effect eHeal = EffectHeal(hpHeal);
        ApplyEffectToObject(DURATION_TYPE_INSTANT, eHeal, oPet);
    }
}