// i_mca_legend11_ac
// by Sabranic
// 7/16/16

// Distributed with MotB Makeover SoZ Edition by Brendan Bellina
// Create 5 Exploding Arrows
// This script is executed automatically when the item is activated
// To trigger properly the name of the script MUST be "i_" + itemtag + "_ac"


void main()
{
	object oPC = GetItemActivator();
	object oITEM = CreateItemOnObject("mca_legend11a", oPC, 5, "mca_legend11a", 0);
}