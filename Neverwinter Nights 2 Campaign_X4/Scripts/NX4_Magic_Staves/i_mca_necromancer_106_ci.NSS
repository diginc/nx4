// OnSpellCastAt script for rechargeable item with 50 charges
// by Brendan Bellina
// Sept 2009

// OnCastSpellAt handler
// This script is executed automatically when a spell is cast at the item
// To trigger properly the name of the script MUST be "i_" + itemtag + "_ci"

// Regulates item recharging (based in part on OC script i_nw_wmgst003_ci)

// Local variable Maximum_Charges can be used to limit the number of charges. If set
// to 0 or not set then the default maximum is 50. If set to -1 then there is no
// maximum. If set greater than 0 then that is the maximum.

void main()
{
	//PrettyDebug("Item OnSpellCastAt script");

	object oItem = GetSpellTargetObject();
	int nCurrentCharges = GetItemCharges(oItem);
	int nDefaultMaxCharges = 50; // default max charges if local int Maximum_Charges not > 0
	int nMaximumCharges = GetLocalInt(oItem, "Maximum_Charges");
	if (nMaximumCharges == 0)
		nMaximumCharges = nDefaultMaxCharges;

	if (nMaximumCharges == -1 || nCurrentCharges < nMaximumCharges)
	{
		int nSpell2DARowNum = GetSpellId();
		int nSpellLevel = StringToInt(Get2DAString("spells", "Wiz_Sorc", nSpell2DARowNum));
	
		if (nSpellLevel == 0)
		{
			nSpellLevel = StringToInt(Get2DAString("spells", "Innate", nSpell2DARowNum));
		}
		if (nMaximumCharges != -1 && nCurrentCharges + nSpellLevel > nMaximumCharges)
			SetItemCharges(oItem, nMaximumCharges);
		else	
			SetItemCharges(oItem, nCurrentCharges + nSpellLevel);			
	}	
}