/*
	Action:
	This will reset all the locals variables for the next new game
*/
// a_jdar_tiro_reset

void main()
{
	// Find a target by Tag - also supports "$PC", "$OWNED_CHAR", etc. (see list in ginc_param_const)
	// Optional second parameter can be added to change the default target.
	object oTarget = OBJECT_SELF;

	SetLocalInt(oTarget, "bActivo", 0);
	SetLocalInt(oTarget, "iRonda", 0);
	SetLocalInt(oTarget, "iTurno", 0);
	SetLocalInt(oTarget, "iJug", 0);
	SetLocalInt(oTarget, "iRond_Max", 0);
	
	SetLocalInt(oTarget, "iPunt1", 0);
	SetLocalInt(oTarget, "iPunt2", 0);
	SetLocalInt(oTarget, "iPunt3", 0);
	SetLocalInt(oTarget, "iPunt4", 0);
	SetLocalInt(oTarget, "iPunt5", 0);
	SetLocalInt(oTarget, "iPunt6", 0);
	SetLocalInt(oTarget, "iPunt7", 0);
	SetLocalInt(oTarget, "iPunt8", 0);
	SetLocalInt(oTarget, "iPunt9", 0);
	SetLocalInt(oTarget, "iPunt10", 0);	
}