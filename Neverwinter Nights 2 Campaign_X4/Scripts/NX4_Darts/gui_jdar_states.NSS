// gui_jdar_states
/*
	Description:
	This script will run all the events for the 3 GUI Panelse
	Register
	Player
	DM 
	
*/
// Created by: Belsirk
// Created:   25/10/2009
// Modified:  25/10/2009
#include "inc_jdar_libreria"


/* ******************************
   *********** Funciones ********
   ****************************** */
   
   
void  RegistroJugador(string sAlias)
{	
	//Must be valid because the gui only can be fire by the conversation
	// this object have
	object oTarget = GetNearestObjectByTag("jdar_tiro"); 
	int iMaxJug;
	int iJugs;
	string sJug;
	string Msj1 = "The player ";
	object oItem;
	int NumDard;
	
	NumDard = GetLocalInt(oTarget, "iRond_Max");	
	iJugs = GetLocalInt(oTarget, "iRonda");
	iMaxJug = GetLocalInt(oTarget, "iJug");
	oItem = CreateItemOnObject("jdar_dard", OBJECT_SELF,NumDard+iExtraDarts,"",TRUE);
	sJug = "<color=Cyan>Gaming Dart "+ sAlias + " </color>";
	SetFirstName(oItem,sJug);
		
	//DEBUG SendMessageToPC(GetFirstPC(), "iJugs GUI= " + IntToString(iJugs));
	
	
	SetLocalString(oItem, "sNombre", sAlias);
	SetLocalInt(oItem, "bEye1", FALSE);
	SetLocalInt(oItem, "bEye2", FALSE);
	SetLocalInt(oItem, "bOHand", FALSE);
			
	
	DeleteLocalInt(oTarget,"bBusy");

	iJugs++;
	RegistrarJugador(oTarget,iJugs, sAlias);
	
	if(iJugs == iMaxJug)
	{
		Msj1 += sAlias + " has been registered, the game can begin!";
		SetLocalInt(oTarget, "iRonda", 1);
		SetLocalInt(oTarget, "iTurno", 1);
		SetLocalInt(oTarget, "bActivo", 2);
		
		
	}
	else
	{
		Msj1 += sAlias +  " has been registered, next player please"; 
		SetLocalInt(oTarget, "iRonda", iJugs);
		
	}
	
	//Reset the current Handicap and score (Should be deleted but better be sure)
	DeleteLocalInt(oTarget, "iHandi"+IntToString(iJugs));
	DeleteLocalInt(oTarget, "iPunt"+IntToString(iJugs));
	
	SendChatMessage(oTarget,OBJECT_SELF,CHAT_MODE_TALK , Msj1, TRUE);
	
	//We close the GUI
	CloseGUIScreen(OBJECT_SELF,"SCREEN_INPUTNAME");
	
	return;
}


void Actualizar_Handicap(string sVar, string sValue)
{

	int iVar;
	object oItem = GetItemPossessedBy(OBJECT_SELF, "jdar_dard");
	object oPC = OBJECT_SELF;
	object oTarget = GetNearestObjectByTag("jdar_tiro"); 
	
	//a_OUT_Handicap(oItem,oPC,oTarget);
	
	SetLocalInt(oItem, sVar, (sValue=="TRUE")?(TRUE):(FALSE));
	
	//Now send back the info 
	if ( sVar == "bOHand")
	{
		if  (sValue=="TRUE")
			a_PUT_Handicap1_OffHand(oItem,oPC,oTarget);
		else
			a_OUT_Handicap1_OffHand(oItem,oPC,oTarget);
			
		SetLocalGUIVariable( oPC,"SCREEN_PLAYERSINFO",0, (sValue=="TRUE") ? "true" : "false");
		
	}
	else if ( sVar == "bEye2")
	{	
		if  (sValue=="TRUE")
		{	
			a_PUT_Handicap1_Eye2(oItem,oPC,oTarget);
			a_OUT_Handicap1_Eye1(oItem,oPC,oTarget);
		}
		else
		{
			a_OUT_Handicap1_Eye2(oItem,oPC,oTarget);
			a_OUT_Handicap1_Eye1(oItem,oPC,oTarget);	
		}
		
		SetLocalGUIVariable( oPC,"SCREEN_PLAYERSINFO",1, (sValue=="TRUE") ? "true" : "false");
		SetLocalGUIVariable( oPC,"SCREEN_PLAYERSINFO",2, "false");
	}
	else if ( sVar == "bEye1")
	{
		
		if  (sValue=="TRUE")
		{
			a_OUT_Handicap1_Eye2(oItem,oPC,oTarget);
			a_PUT_Handicap1_Eye1(oItem,oPC,oTarget);
			
		}
		else
		{	
			a_OUT_Handicap1_Eye1(oItem,oPC,oTarget);
			a_OUT_Handicap1_Eye2(oItem,oPC,oTarget);
			
		}
		SetLocalGUIVariable( oPC,"SCREEN_PLAYERSINFO",2, (sValue=="TRUE") ? "true" : "false");	
		SetLocalGUIVariable( oPC,"SCREEN_PLAYERSINFO",1, "false");
	}
	

}

void RemoveAllHandicap()
{
	object oItem = GetItemPossessedBy(OBJECT_SELF, "jdar_dard");
	object oPC = OBJECT_SELF;
	object oTarget = GetNearestObjectByTag("jdar_tiro"); 
	
	SetLocalGUIVariable( oPC,"SCREEN_PLAYERSINFO",0, "false");
	SetLocalGUIVariable( oPC,"SCREEN_PLAYERSINFO",1, "false");
	SetLocalGUIVariable( oPC,"SCREEN_PLAYERSINFO",2, "false");	
	
	a_OUT_Handicap(oItem, oPC, oTarget);
}


void main(string sInt, string sParm1, string sParm2)
{
	int iState;
	iState = StringToInt(sInt);
	/* DEBUG
	SendMessageToPC(GetFirstPC(),"Estado: " + sInt);
	SendMessageToPC(GetFirstPC(),"Parametro 1: " +  sParm1);
	SendMessageToPC(GetFirstPC(),"Parametro 2: " +  sParm2);//*/
	
	switch(iState)
	{	//REGISTRO
		case 1: RegistroJugador(sParm1); break;
		
		//Handicap
		case 2:Actualizar_Handicap(sParm1, sParm2); break;
		
		//Remove all Handicap
		case 3: RemoveAllHandicap(); break;
		
		//DM Handicap
		case 4: SetLocalInt(GetNearestObjectByTag("jdar_tiro"),"iDMHandi",StringToInt(sParm2)); break;
	
	}
}