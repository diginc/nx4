/*
	Action:
	This one will compare the score of N playres against the Top5 and update all the Scores
	Yes. Ay guy could have the top list with his name with this method 
	

*/
// c_jdar_tiro_sem2

#include "inc_jdar_libreria"

void main()
{

    object oPC = GetPCSpeaker();
	object oArea, oCheer;
	// Find a target by Tag - also supports "$PC", "$OWNED_CHAR", etc. (see list in ginc_param_const)
	// Optional second parameter can be added to change the default target.
	object oTarget = OBJECT_SELF;
	int bRecord = FALSE;
	
	//The mechanism for this will be the most simple, for each player and his score 
	//we compare against the Top5, (From higher to lower) if he have a better score he 
	// is put on the Toplist, only after the last member we annoucement the Players who 
	//made success.
	int iNum = GetLocalInt(oTarget, "iJug");
	int cont;
	string Msj;
	
	//DEBUG
	//SendMessageToPC(oPC, "Esta por ejecutar");
	
	for ( cont = 1; cont <= iNum; cont++)
	{
		if (CompararJugador_VS_Top(oTarget, cont))
			bRecord = TRUE;
	}
	
	//DEBUg
	//SendMessageToPC(oPC, "Resultado: " +IntToString(bRecord));
		
	if(!bRecord) 
		return;
		
	//If we get here, THEN CHEVE GRATIS  FOR EVERYONE in the bar!
	oArea = GetArea(oTarget);
	oCheer = GetFirstPC();
	
	while ( oCheer != OBJECT_INVALID )
	{
		if (GetArea(oCheer) == oArea)
		{
			CreateItemOnObject("NW_IT_MPOTION021", oCheer);
			
		}
	
		//Debug
		//SendChatMessage(oTarget,oPC,CHAT_MODE_TALK, "¡EL HORROR!!");
		
		oCheer = GetNextPC();
	}//End while
	
	//And of course we anoucement everything
	SendChatMessage(oTarget,oPC,CHAT_MODE_TALK, "We have a new record! FREE ALE FOR EVERYONE!");
	SendChatMessage(oTarget,oPC,CHAT_MODE_TALK, "New Top 5:");

	
	for(cont = 5; cont >= 1; cont--)
	{
		Msj = Annoucement_Top(oTarget, cont);
		SendChatMessage(oTarget,oPC,CHAT_MODE_TALK, Msj);
	}
	
}