// i_bb_it_rodmight_staf_ac
// by Brendan Bellina
// July 2008

// bb_it_rodmight_staf OnActivate handler
// This script is executed automatically when the item is activated
// To trigger properly the name of the script MUST be "i_" + itemtag + "_ac"
// Also to make the item usable set Item Property to Cast Spell: Unique Power on Self

// Initiates Rod of Lordly Might conversation

//#include "ginc_debug"
	
void main()
{
	//PrettyDebug("i_bb_it_rodmight_ac: Rod of Lordly Might");

	object oPC = GetItemActivator();
	SetLocalString(oPC, "BB_RODMIGHT_TAG", GetTag(GetItemActivated()));
	SetLocalString(oPC, "BB_RODMIGHT_FORM", "staff");
	SetGlobalString("BB_RODMIGHT_ACTOR", GetTag(oPC));
	// The campaign/module  may not allow companions to converse, so the conversation may
	// be with the FirstPC if the actor is a companion.
	AssignCommand(oPC, ActionStartConversation(oPC, "conv_rodmight", FALSE, FALSE, TRUE, FALSE));
}