//	g15_client_enter
/*
    On Client Enter script for G15.
*/
//	JSH-OEI 6/20/08

#include "ginc_group"

void SpawnCreatures(string sResRef)
{
	string sWPTag = "sp_" + sResRef;
	object oWP = GetObjectByTag(sWPTag);
	int i = 0;
	
	while(GetIsObjectValid(oWP))
	{
		PrettyDebug ("Spawning in " + sResRef);
		if(GetObjectType(oWP) == OBJECT_TYPE_WAYPOINT 
			&& GetArea(oWP) == OBJECT_SELF
			&& !IsMarkedAsDone(oWP))
		{
			location lLoc = GetLocation(oWP);
			object oCreature = CreateObject(OBJECT_TYPE_CREATURE, sResRef, lLoc);
			GroupAddMember("G15 Enemies", oCreature);
			MarkAsDone(oWP);
		}
		
		i++;		
		oWP = GetObjectByTag(sWPTag, i);
	}
}

void CreateLoot(object oPC)
{
	object oSarc = GetNearestObjectByTag("g15_sarcophagus", oPC);

	//gold piece (375), fire opal, bloodstone, ancient trap parts
	CreateItemOnObject("nw_gold_001", oSarc, 375);
	CreateItemOnObject("nw_it_gem009", oSarc);
	CreateItemOnObject("cft_gem_01", oSarc);
	CreateItemOnObject("nx2_t_trapparts", oSarc);
}

void main()
{
	object oPC 		= GetFirstEnteringPC();
		
	SpawnCreatures("g15_skeleton");
	SpawnCreatures("g15_zombie");
	
	if (!IsMarkedAsDone())
	{
		CreateLoot(oPC);
		SetGlobalInt("00_bExploredG15", TRUE);
		MarkAsDone();
	}
}