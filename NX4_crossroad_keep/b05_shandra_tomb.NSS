//	g12_p_sarcophagus_di
/*
	On Inventory Disturbed script for g12_p_sarcophagus.
*/
//	JSH-OEI 6/10/08

#include "ginc_misc"

void SpawnCreatures(string sResRef)
{
	string sWPTag = "sp_" + sResRef;
	object oWP = GetObjectByTag(sWPTag);
	int i = 0;
	
	while(GetIsObjectValid(oWP))
	{
		PrettyDebug ("Spawning in " + sResRef);
		if(GetObjectType(oWP) == OBJECT_TYPE_WAYPOINT && !IsMarkedAsDone(oWP))
		{
			location lLoc = GetLocation(oWP);
			object oCreature = CreateObject(OBJECT_TYPE_CREATURE, sResRef, lLoc);
			MarkAsDone(oWP);
		}
		
		i++;		
		oWP = GetObjectByTag(sWPTag, i);
	}
}

void main()
{
	SpawnCreatures("c_fiend");
}