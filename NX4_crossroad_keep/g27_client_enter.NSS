//	g27_client_enter
/*
	On Client Enter for G27 Zecorian's Demesne, Level 3.
*/
//	JSH-OEI 8/11/08

#include "ginc_group"
#include "ginc_misc"

void SpawnCreaturesAtWaypoints(string sResRef)
{
	string sWPTag = "sp_" + sResRef;
	object oWP = GetObjectByTag(sWPTag);
	int i = 0;
	
	while(GetIsObjectValid(oWP))
	{
		PrettyDebug ("Spawning in " + sResRef);
		if(GetObjectType(oWP) == OBJECT_TYPE_WAYPOINT 
			&& GetArea(oWP) == OBJECT_SELF
			&& !IsMarkedAsDone(oWP))
		{
			location lLoc = GetLocation(oWP);
			object oCreature = CreateObject(OBJECT_TYPE_CREATURE, sResRef, lLoc);
			GroupAddMember("G27 Enemies", oCreature);
			MarkAsDone(oWP);
		}
		
		i++;		
		oWP = GetObjectByTag(sWPTag, i);
	}
}

//creates loot on chests//
void CreateLoot(object oPC)
{
	object oChest		= GetNearestObjectByTag("g24_chest", oPC);
	object oBookcase	= GetNearestObjectByTag("g24_bookcase", oPC);	
	
	CreateItemOnObject("g24_journal", oBookcase);		// Journal
	
	CreateItemOnObject("NW_IT_GOLD001", oChest, 250);	// Gold
	CreateItemOnObject("NW_IT_MNECK023", oChest);		// Glittering Necklace
	CreateItemOnObject("X1_IT_SPARSCR301", oChest);		// Scroll of Displacement
}

void main()
{
	object oPC			= GetFirstEnteringPC();
	
	SpawnCreaturesAtWaypoints("g27_zecorian");
	SpawnCreaturesAtWaypoints("g27_vampire_lord_1");
	SpawnCreaturesAtWaypoints("g27_vampire_lord_2");
	CreateLoot(oPC);
}