//	g25_b_skull_hb
/*
	The giant skull likes to look at creatures, and will turn to face a
	random living creature in the area.
*/
//	JSH-OEI 8/12/08

void main()
{
	object oTarget	= GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_IS_CONTROLLED);
		
	SetFacingPoint(GetPosition(oTarget));
}